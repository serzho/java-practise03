package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;
import static org.mockito.Mockito.*;
import java.util.*;

public class AuditTest {

    SkiPass coolPass1 = null;
    SkiPass coolPass2 = null;
    SkiPass failed = null;
    Turnstile turnstile = null;

    @Before
    public void setUp() {
        coolPass1 = new EmptySkiPass();
        coolPass2 = new EmptySkiPass();
        failed = spy(new EmptySkiPass());
        when(failed.use()).thenReturn(false);

        turnstile = new Turnstile();
        turnstile.trySkiPass(coolPass1);
        turnstile.trySkiPass(coolPass2);
        turnstile.trySkiPass(failed);
    }

    @Test
    public void test_should_return_list_of_maps_with_audit() {

        List<Map<String, String>> audit = turnstile.audit.audit;
        assertThat(audit.size(), is(3));

        Map<String, String> firstAudit = audit.get(0);

        assertThat(firstAudit.get("uid"), is(coolPass1.uid()));
        assertThat(firstAudit.get("type"), is("test"));
        assertThat(firstAudit.get("passed"), is("true"));
        assertThat(firstAudit.get("timestamp"), is(notNullValue()));
    }

    @Test
    public void test_return_audit_stats_for_all_passes() {

        Map<String, Integer> result = turnstile.audit.forAll();

        assertThat(result.get("passed"), is(2));
        assertThat(result.get("failed"), is(1));
    }

    @Test
    public void test_return_audit_stats_for_some_type_of_passes() {
        SkiPass other = spy(new EmptySkiPass());
        when(other.type()).thenReturn("other");
        turnstile.trySkiPass(other);

        Map<String, Integer> result = turnstile.audit.forType("test");

        assertThat(result.get("passed"), is(2));
        assertThat(result.get("failed"), is(1));
    }
}
