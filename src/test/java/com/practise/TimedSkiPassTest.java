package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;

public class TimedSkiPassTest {

    @Test
    public void test_is_valid() {
        TimedSkiPass pass = new TimedSkiPass();

        pass.timeRange = TimeRange.forADay(DateTime.now().plusDays(-1));
        assertThat(pass.isValid(), is(false));

        pass.timeRange = TimeRange.forADay(DateTime.now());
        assertThat(pass.isValid(), is(true));
    }
}
