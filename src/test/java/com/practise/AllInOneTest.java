package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;
import static org.mockito.Mockito.*;
import java.util.*;

public class AllInOneTest {

    @Test
    public void all_in_one() {
        
        SkiPass for5DaysFromToday = PassSystem.skiPassForFiveDays(DateTime.now());
        SkiPass for1DayFromTomorow = PassSystem.skiPassForADay(DateTime.now().plusDays(1));
        SkiPass for20Rides = PassSystem.skiPassForTwentyRides();
        SkiPass for10Rides = PassSystem.skiPassForTenRides();

        Turnstile firstTurnstile = new Turnstile();
        Turnstile secondTurnstile = new Turnstile();

        assertThat(firstTurnstile.trySkiPass(for5DaysFromToday), is(true));
        assertThat(firstTurnstile.trySkiPass(for20Rides), is(true));

        assertThat(secondTurnstile.trySkiPass(for1DayFromTomorow),
                   is(false));

        for (int i = 0; i < 10; i++)
            assertThat(secondTurnstile.trySkiPass(for10Rides), is(true));
        assertThat(secondTurnstile.trySkiPass(for10Rides), is(false));

        PassSystem.blockSkiPass(for5DaysFromToday.uid());
        assertThat(firstTurnstile.trySkiPass(for5DaysFromToday), is(false));

        Map<String, Integer> audit = firstTurnstile.auditForAll();
        assertThat(audit.get("passed"), is(2));
        assertThat(audit.get("failed"), is(1));

        Map<String, Integer> auditByType = secondTurnstile.auditForType(for20Rides.type());
        assertThat(auditByType.get("passed"), is(10));
        assertThat(auditByType.get("failed"), is(1));
    }
}
