package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;
import static org.mockito.Mockito.*;

public class PassSystemTest {

    @Test
    public void test_find_turnstile() {
        Turnstile turnstile = new Turnstile();

        Turnstile found = PassSystem.findTurnstile(turnstile.uid());
        assertThat(found, notNullValue());
        assertThat(found, equalTo(turnstile));

        found = PassSystem.findTurnstile("bad uid");
        assertThat(found, nullValue());
    }

    @Test
    public void test_find_skipass() {
        SkiPass pass = new EmptySkiPass();

        SkiPass found = PassSystem.findSkiPass(pass.uid());
        assertThat(found, notNullValue());
        assertThat(found, equalTo(pass));

        found = PassSystem.findSkiPass("bad uid");
        assertThat(found, nullValue());
    }

    @Test
    public void test_block_skipass() {
        SkiPass pass = new EmptySkiPass();
        String uid = pass.uid();

        PassSystem.blockSkiPass(uid);
        assertThat(pass.blocked, is(true));
    }
}
