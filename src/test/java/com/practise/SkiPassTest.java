package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;

public class SkiPassTest {
    EmptySkiPass pass;

    @Before
    public void setUp() {
        pass = new EmptySkiPass();
    }

    @Test
    public void test_uid_generating() {
        assertThat(pass.uid, notNullValue());
    }

    @Test
    public void test_equals() {
        EmptySkiPass other = new EmptySkiPass();
        assertThat(other, not(equalTo(pass)));

        other.uid = pass.uid;
        assertThat(other, equalTo(pass));
    }

    @Test
    public void test_should_be_created_not_blocked() {
        assertThat(pass.blocked, is(false));
    }
}
