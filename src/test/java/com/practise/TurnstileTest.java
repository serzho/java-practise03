package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;
import static org.mockito.Mockito.*;
import java.util.*;

public class TurnstileTest {

    Turnstile turnstile;

    @Before
    public void setUp() {
        turnstile = new Turnstile();
    }

    @Test
    public void test_equals_method() {
        Turnstile other = new Turnstile();
        assertThat(turnstile, not(equalTo(other)));

        other.uid = turnstile.uid;
        assertThat(turnstile, equalTo(other));
    }

    @Test
    public void test_try_ski_pass() {
        RideSkiPass pass = new RideSkiPass(0);

        assertThat(turnstile.trySkiPass(pass), is(false));
    }

    @Test
    public void test_uid_should_not_be_null() {
        assertThat(turnstile.uid, notNullValue());
    }

    @Test
    public void test_on_create_should_add_self_to_pass_system() {
        assertThat(PassSystem.turnstiles, hasItem(turnstile));
    }
}
