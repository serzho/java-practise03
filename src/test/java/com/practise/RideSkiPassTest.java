package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;

public class RideSkiPassTest {
    
    @Test
    public void test_is_valid() {
        RideSkiPass pass = new RideSkiPass();

        pass.times = 0;
        assertThat(pass.isValid(), is(false));

        pass.times = 2;
        assertThat(pass.isValid(), is(true));
    }

    @Test
    public void test_use() {
        RideSkiPass pass = new RideSkiPass();

        pass.times = 5;
        assertThat(pass.use(), is(true));
        assertThat(pass.times, is(4));

        pass.times = 0;
        assertThat(pass.use(), is(false));
        assertThat(pass.times, is(0));
    }
}
