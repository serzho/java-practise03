package com.practise;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static com.practise.TimeRange.*;
import org.joda.time.*;
import static com.practise.PassSystem.*;

public class TimeRangeTest {
    TimeRange range;

    @Before
    public void before() {
        range = null;
    }

    @Test
    public void test_first_half_of_day() {
        range = firstHalfOfDay(DateTime.now());
        assertThat(range.inRange(DateTime.now().withHourOfDay(10)), is(true));

        range = firstHalfOfDay(DateTime.now());
        assertThat(range.inRange(DateTime.now().withHourOfDay(14)), is(false));
    }

    @Test
    public void test_second_half_of_day() {
        range = secondHalfOfDay(DateTime.now());
        assertThat(range.inRange(DateTime.now().withHourOfDay(14)), is(true));

        range = secondHalfOfDay(DateTime.now());
        assertThat(range.inRange(DateTime.now().withHourOfDay(10)), is(false));
    }

    @Test
    public void test_for_day() {
        range = forADay(DateTime.now());
        assertThat(range.inRange(), is(true));

        range = forADay(DateTime.now().plusDays(1));
        assertThat(range.inRange(), is(false));
    }
    @Test
    public void test_for_two_days() {
        range = forTwoDays(DateTime.now());
        assertThat(range.inRange(), is(true));

        range = forTwoDays(DateTime.now().plusDays(3));
        assertThat(range.inRange(), is(false));
    }
    @Test
    public void test_for_five_days() {
        range = forFiveDays(DateTime.now());
        assertThat(range.inRange(), is(true));

        range = forFiveDays(DateTime.now().plusDays(6));
        assertThat(range.inRange(), is(false));
    }
}
