package com.practise;

import org.joda.time.*;

public class TimeRange {
    protected DateTime first;
    protected DateTime second;

    protected TimeRange() {}

    public static TimeRange firstHalfOfDay(DateTime day) {
        TimeRange range = new TimeRange();
        range.first = new DateTime(day).withHourOfDay(9);
        range.second = new DateTime(day).withHourOfDay(13);

        return range;
    }

    public static TimeRange secondHalfOfDay(DateTime day) {
        TimeRange range = new TimeRange();
        range.first = new DateTime(day).withHourOfDay(13);
        range.second = new DateTime(day).withHourOfDay(17);

        return range;
    }

    public static TimeRange forADay(DateTime day) {
        TimeRange range = new TimeRange();
        range.first = new DateTime(day).withTimeAtStartOfDay();
        range.second = new DateTime(day).plusDays(1).withTimeAtStartOfDay();

        return range;
    }

    public static TimeRange forTwoDays(DateTime day) {
        TimeRange range = new TimeRange();
        range.first = new DateTime(day).withTimeAtStartOfDay();
        range.second = new DateTime(day).plusDays(2).withTimeAtStartOfDay();

        return range;
    }

    public static TimeRange forFiveDays(DateTime day) {
        TimeRange range = new TimeRange();
        range.first = new DateTime(day).withTimeAtStartOfDay();
        range.second = new DateTime(day).plusDays(5).withTimeAtStartOfDay();

        return range;
    }

    public boolean inRange(DateTime date) {
        return date.isAfter(first) &&
               date.isBefore(second);
    }

    public boolean inRange() {
        return DateTime.now().isAfter(first) &&
               DateTime.now().isBefore(second);
    }
}
