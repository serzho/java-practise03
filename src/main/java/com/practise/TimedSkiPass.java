package com.practise;

import org.joda.time.*;

public class TimedSkiPass extends SkiPass {

    protected TimeRange timeRange = TimeRange.forADay(DateTime.now());

    protected TimedSkiPass(TimeRange timeRange) {
        super();
        this.timeRange = timeRange;
    }

    protected TimedSkiPass() {
        super();
    }

    @Override
    public boolean isValid() {
        boolean timeInRange = timeRange.inRange();
        if (timeInRange)
            return true;
        else
            return false;
    }

    @Override
    public boolean use() {
        return true;
    }

    @Override
    public String type() {
        return "timed";
    }
}
