package com.practise;

import static org.joda.time.DateTimeConstants.*;
import org.joda.time.*;

import java.util.*;

public class PassSystem {
    
    private static PassSystem instance;

    protected static List<Turnstile> turnstiles = new ArrayList();

    protected static List<SkiPass> skiPasses = new ArrayList();

    public static void addTurnstile(Turnstile t) {
        turnstiles.add(t);
    }

    public static Turnstile findTurnstile(String uid) {
        for (Turnstile turnstile : turnstiles) 
            if (turnstile.uid().equals(uid))
                return turnstile;
        return null;
    }

    public static void addSkiPass(SkiPass pass) {
        skiPasses.add(pass);
    }

    public static SkiPass findSkiPass(String uid) {
        for (SkiPass skiPass : skiPasses)
            if (skiPass.uid().equals(uid))
                return skiPass;
        return null;
    }

    public static void blockSkiPass(String uid) {
        findSkiPass(uid).block();
    }

    public static SkiPass skiPassForTenRides() {
        return new RideSkiPass(10);
    }

    public static SkiPass skiPassForTwentyRides() {
        return new RideSkiPass(20);
    }

    public static SkiPass skiPassForFiftyRides() {
        return new RideSkiPass(50);
    }

    public static SkiPass skiPassForHundredRides() {
        return new RideSkiPass(100);
    }

    public static SkiPass skiPassForFirstHalfOfDay(DateTime date) {
        return new TimedSkiPass(TimeRange.firstHalfOfDay(date));
    }

    public static SkiPass skiPassForSecondHalfOfDay(DateTime date) {
        return new TimedSkiPass(TimeRange.secondHalfOfDay(date));
    }

    public static SkiPass skiPassForADay(DateTime date) {
        return new TimedSkiPass(TimeRange.forADay(date));
    }

    public static SkiPass skiPassForTwoDays(DateTime date) {
        return new TimedSkiPass(TimeRange.forTwoDays(date));
    }

    public static SkiPass skiPassForFiveDays(DateTime date) {

        boolean nowIsHoliday = date.dayOfWeek().get() == SATURDAY ||
                               date.dayOfWeek().get() == SUNDAY;

        if (!nowIsHoliday)
            return new TimedSkiPass(TimeRange.forFiveDays(date));
        else
            return new TimedSkiPass();
    }
}
