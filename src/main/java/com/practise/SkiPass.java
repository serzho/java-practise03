package com.practise;

import java.util.*;
import org.joda.time.*;

public abstract class SkiPass {

    protected String uid;
    protected boolean blocked = false;
    protected DateTime expiredTime;
    
    protected SkiPass() {
        expiredTime = DateTime.now()
            .withTimeAtStartOfDay().plusMonths(6);
        uid = UUID.randomUUID().toString();
        PassSystem.addSkiPass(this);
    }

    public abstract boolean isValid();

    public abstract boolean use();

    public String uid() {
        return uid;
    }

    public abstract String type();

    public boolean isNotBlocked() {
        return !blocked;
    }

    public boolean isNotExpired() {
        return DateTime.now().isBefore(expiredTime);
    }

    protected void block() {
        blocked = true;
    }
    public boolean equals(Object other) {
        if (other == null) return false;
        if (!other.getClass().equals(this.getClass())) return false;
        SkiPass t = (SkiPass) other;
        if (other == this) return true;
        if (!t.uid().equals(this.uid())) return false;
        return true;
    }
}
