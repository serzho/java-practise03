package com.practise;

import java.util.*;

public class Audit {

    protected List<Map<String, String>> audit;

    public Audit() {
        audit = new ArrayList();
    }

    public void log(String uid, String type,
                    long timestamp, boolean passed) {

        Map<String, String> auditEntry = new HashMap();
        auditEntry.put("uid", uid);
        auditEntry.put("type", type);
        auditEntry.put("timestamp", String.valueOf(timestamp));
        auditEntry.put("passed", String.valueOf(passed));
        audit.add(auditEntry);
    }

    public Map<String, Integer> forAll() {
        int passed = 0;
        for (Map<String, String> entry : audit)
            if (entry.get("passed").equals("true"))
                passed++;

        return result(passed, audit.size() - passed);
    }

    public Map<String, Integer> forType(String type) {
        int passed = 0;
        int failed = 0;
        for (Map<String, String> entry : audit)
            if (entry.get("type").equals(type))
                if (entry.get("passed").equals("true"))
                    passed++; 
                else
                    failed++;

        return result(passed, failed);
    }

    protected Map<String, Integer> result(int passed, int failed) {
        Map<String, Integer> result = new HashMap();
        result.put("passed", passed);
        result.put("failed", failed);

        return result;
    }
}
