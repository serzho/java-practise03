package com.practise;

import java.util.*;

public class Turnstile {
    
    protected String uid;
    protected Audit audit;

    public Turnstile () {
        uid = UUID.randomUUID().toString();
        audit = new Audit();
        PassSystem.addTurnstile(this);
    }

    public String uid() {
        return uid;
    }

    public boolean trySkiPass(SkiPass pass) {
        boolean result;
        if (pass.isValid() && pass.isNotBlocked() && 
            pass.isNotExpired() && pass.use())

            result = true;
        else
            result = false;

        audit.log(pass.uid(), pass.type(),
                  System.currentTimeMillis(),
                  result);
        return result;
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        if (!other.getClass().equals(this.getClass())) return false;
        Turnstile t = (Turnstile) other;
        if (other == this) return true;
        if (!t.uid().equals(this.uid())) return false;
        return true;
    }

    public Map<String, Integer> auditForAll() {
        return audit.forAll();
    }

    public Map<String, Integer> auditForType(String type) {
        return audit.forType(type);
    }

}
