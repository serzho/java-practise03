package com.practise;

import org.joda.time.*;

public class RideSkiPass extends SkiPass {

    protected int times;

    protected RideSkiPass() {
        super();
    } 

    protected RideSkiPass(int times) {
        super();
        this.times = times;
    }

    @Override
    public boolean isValid() {
        return times > 0;  
    }

    @Override
    public boolean use() {
        if (times > 0) {
            times--;
            return true;
        } else 
            return false;
    }

    @Override
    public String type() {
        return "rides";
    }
}
