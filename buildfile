# Generated by Buildr 1.4.7, change to your liking


# Version number for this release
VERSION_NUMBER = "1.0.0"
# Group identifier for your projects
GROUP = "practise03"
COPYRIGHT = ""

# Specify Maven 2.0 remote repositories here, like this:
repositories.remote << "http://repo1.maven.org/maven2"

desc "The Practise03 project"
define "practise03" do

  project.version = VERSION_NUMBER
  project.group = GROUP
  manifest["Implementation-Vendor"] = COPYRIGHT
  compile.with 'joda-time:joda-time:jar:2.1'
  test.using :junit
  test.with 'org.hamcrest:hamcrest-all:jar:1.3',
            'org.mockito:mockito-all:jar:1.9.0'
end

